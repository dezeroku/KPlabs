/**Prostokat
@author d0ku (Jakub Piątkowski)
@version 1.0
*/


using namespace std;


class Prostokat
: public Czworokat {




	Prostokat(double a, double b){
		this->a=a;
		this->b=b;
		this->pole_licz();
		this->obwod_licz();

	}

	private:

	 void pole_licz(){
		this->pole=this->a*this->b;
	}

	void obwod_licz(){
		this->obwod =2*this->a + 2*this->b;
	}

	public: 

	double obwod_zwroc(){
		return this->obwod;
	}

	double pole_zwroc(){
		return this->pole;
	}

};