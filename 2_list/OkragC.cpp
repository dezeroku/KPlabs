/**Okrag
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

#include <math.h>

using namespace std;

class Okrag 
: public Figura{

	private:
	 double r;

	Okrag(double r){

		string a_1="Promien musi byc wiekszy niz zero ";
		if(r<=0)
			throw a_1;

		this->r=r;
		pole_licz();
		obwod_licz();
	}

	private:
	 void pole_licz(){

		this->pole=M_PI*r*r;
	}

	void obwod_licz(){
		this->obwod=2*M_PI*r;

	}

	public:
	double obwod_zwroc(){

		return this->obwod;
	}

	double pole_zwroc(){

		return this->pole;
	}

};