/** Test class, connector beetwen all subclasses
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

public class Test{


	public static void main(String[] args) {

		int count =0;
		double f=0,b=0,c=0,d=0,e=0;

		char[] figury= args[0].toCharArray();

		for (int i=0;i<figury.length;i++){

			if((figury[i]=='o')||(figury[i]=='p')||(figury[i]=='s'))
				count++;
			if(figury[i]=='c')
				count=count+5;


		}

		if(count!=args.length-1)
			throw new IllegalArgumentException("Niewystarczajaca ilosc parametrow, brakuje: "+ (count-args.length+1));

		count =1;

		Figura[] tab = new Figura[figury.length];

		for (int i=0;i<figury.length;i++){
		
			if(figury[i]=='o'){

				f=Test.parsowanie(args[count]);
				Okrag temp =new Okrag(f);

				tab[i]=temp;
				count++;
				temp=null;

			}
			else if(figury[i]=='c'){

				f=Test.parsowanie(args[count]);
				b=Test.parsowanie(args[count+1]);
				c=Test.parsowanie(args[count+2]);
				d=Test.parsowanie(args[count+3]);
				e=Test.parsowanie(args[count+4]);
				Czworokat temp = new Czworokat();

				temp.czworokat_maker(f,b,c,d,e);
				tab[i]=temp;
				count=count+5;
				temp=null;
			}
			else if(figury[i]=='p'){
				f=Test.parsowanie(args[count]);
				Pieciokat temp = new Pieciokat(f);

				tab[i]=temp;
				count++;
				temp=null;
			}
			else if (figury[i]=='s'){
				f=Test.parsowanie(args[count]);
				Szesciokat temp = new Szesciokat(f);
				
				tab[i]=temp;
				count++;
				temp=null;
			}

		}


		for (int i=1;i<figury.length+1;i++)
			{
				System.out.println("Pole "+i+" figury wynosi "+tab[i-1].pole());
				System.out.println("Obwod "+i+" figury wynosi "+tab[i-1].obwod());


			}

	}

	private static double parsowanie(String a){
		double z=0;

		try{z=Double.parseDouble(a);}
		catch(NumberFormatException ex){
			System.out.println("To nie jest liczba: "+a);
		}

		return z;
	}

}