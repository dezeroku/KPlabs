/**Kwadrat
@author d0ku (Jakub Piątkowski)
@version 1.0
*/



class Kwadrat 
: public Czworokat{

	Kwadrat(double a){
		this->a=a;
		this->pole_licz();
		this->obwod_licz();
	}

	private:

	void pole_licz(){
		this->pole=this->a*this->a;
	}

	void obwod_licz(){
		this->obwod=this->a*4;
	}

	public:

	double pole(){
		return this->pole;
	}

	double obwod(){
		return this->obwod;
	}

};