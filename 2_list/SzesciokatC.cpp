/**Szesciokat
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

class Szesciokat 
: public Figura {

	private:
	 double r;

	Szesciokat(double r){

		string a_1="Bok szesciokata musi byc wiekszy niz zero ";

		if(r<=0)
			throw a_1;

		this->r=r;
		this->pole_licz();
		this->obwod_licz();
	}

	private:

	void pole_licz(){

		this->pole =((this->r*this->r)*sqrt(3))*1.5;
	}

	void obwod_licz(){

		this->obwod=this->r*6;
	}

	public:

	double obwod_zwroc(){
		return this->obwod;
	}

	double pole_zwroc(){
		return this->pole;
	}

};