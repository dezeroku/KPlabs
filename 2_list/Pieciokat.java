/**Pieciociokat
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

public class Pieciokat extends Figura{

	private double r;

	Pieciokat(double r){

		if(r<=0)
			throw new IllegalArgumentException("Bok pieciokata musi byc wiekszy niz zero : " + r);

		this.r=r;
		this.pole_licz();
		this.obwod_licz();
	}

	private void pole_licz(){

		this.pole =((this.r*this.r)*(Math.sqrt(5*(5+2*Math.sqrt(5)))))*0.25;
	}

	private void obwod_licz(){

		this.obwod=this.r*5;
	}

	public double obwod(){
		return this.obwod;
	}

	public double pole(){
		return this.pole;
	}

}