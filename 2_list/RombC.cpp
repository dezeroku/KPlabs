/**Romb
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

#include <math.h>

class Romb 
: public Czworokat{


	Romb(double a, double angle){
		this->a=a;
		this->angle=angle;
		this->pole_licz();
		this->obwod_licz();
	}

	private:

	void pole_licz(){
		if (this->angle==30) 
			this->pole=this->a*this->a*0.5;
			else
		this->pole=this->a*this->a*sin(this->angle*PI/180);

	}

	void obwod_licz(){

		this->obwod=this->a*4;
	}

	public:

	double obwod_zwroc(){
		return this->obwod;
	}

	double pole_zwroc(){

		return this->pole;
	}

};