/**Figura, abstract class
@author d0ku (Jakub Piątkowski)
@version 1.0
*/


import java.lang.*;


abstract class Figura{

	protected double pole;
	protected double obwod;

	abstract double pole();
	abstract double obwod();

}













