/**FiguraC, contains abstract Figura , all subclasses and main();
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

#include <iostream>
#include <cmath>
#include <string>
#include <math.h>
#include <stdlib.h>
using namespace std;


class Figura{

	protected:
	double pole;
	double obwod;

	public:

	virtual double pole_zwroc(){return this->pole;};
	virtual double obwod_zwroc(){return this->obwod;};
	 ~Figura(){};
	 Figura(){};

};

class Czworokat 
: public Figura {

	protected:
	double a;
	double b;
	double c;
	double d;
	double angle;

	public:
	 
	 	void czworokat_maker(double a, double b, double c, double d, double angle);

		double pole_zwroc(){

			return this->pole;
		}

		double obwod_zwroc(){

			return this->obwod;
		}


};




class Prostokat
: public Czworokat {



	public:

	Prostokat(double a, double b){
		this->a=a;
		this->b=b;
		this->pole_licz();
		this->obwod_licz();

	}

	private:

	 void pole_licz(){
		this->pole=this->a*this->b;
	}

	void obwod_licz(){
		this->obwod =2*this->a + 2*this->b;
	}

	public: 

	double obwod_zwroc(){
		return this->obwod;
	}

	double pole_zwroc(){
		return this->pole;
	}

};

class Kwadrat 
: public Czworokat{

	public:
		Kwadrat(double a){
		this->a=a;
		this->pole_licz();
		this->obwod_licz();
	}

	private:

	void pole_licz(){
		this->pole=this->a*this->a;
	}

	void obwod_licz(){
		this->obwod=this->a*4;
	}

	public:

	double pole_zwroc(){
		return this->pole;
	}

	double obwod_zwroc(){
		return this->obwod;
	}

};

class Romb 
: public Czworokat{

	public:

	Romb(double a, double angle){
		this->a=a;
		this->angle=angle;
		this->pole_licz();
		this->obwod_licz();
	}

	double obwod_zwroc(){
		return this->obwod;
	}

	double pole_zwroc(){

		return this->pole;
	}
	private:

	void pole_licz(){
		if (this->angle==30) 
			this->pole=this->a*this->a*0.5;
			else
		this->pole=this->a*this->a*sin(this->angle*M_PI/180);

	}

	void obwod_licz(){

		this->obwod=this->a*4;
	}


	

};


void Czworokat::czworokat_maker(double a, double b, double c, double d, double angle){

	 	double max=0;
 
 		string a_1="Te liczby nie spelniaja warunku czworokata";
 		string a_2="Jeden z bokow czworokata jest mniejszy od zera";
 		string a_3="Kat czworokata jest bledny: ";

 		if(a>max)
 			max=a;
 		if(b>max)
 			max=b;
 		if(c>max)
 			max=c;
 		if(d>max)
 			max=d;

		if(!( max < a + b + c + d - max)){
			cout <<a_1<<endl;
			throw a_1;
		}

		if(a<=0 || b<=0 || c<=0 || d<=0){
			cout <<a_2<<endl;
			throw a_2;
		}

		if(angle<=0 || angle>=180){
			cout << a_3<<endl;
			throw a_3;
		}

		if(a==c && b==d && a==b && angle==90){     //warunek na kwadrat
			Kwadrat *kwadrat=new Kwadrat(a);
			this->obwod=kwadrat->obwod_zwroc();
			this->pole=kwadrat->pole_zwroc();				
		}



		if(a==c && b==d && angle==90){ //warunek na prostokat nr 1
			Prostokat *prostokat = new Prostokat(a,b);
			this->pole=prostokat->pole_zwroc();
			this->obwod=prostokat->obwod_zwroc();
		}

		else if (a==b && c==d && angle==90){ // warunek na prostokat nr 2

			Prostokat *prostokat = new Prostokat(a,c);
			this->pole=prostokat->pole_zwroc();
			this->obwod=prostokat->obwod_zwroc();
		}

		else if (a==d && c==b && angle==90){ // warunek na prostokat nr 2

			Prostokat *prostokat = new Prostokat(a,c);
			this->pole=prostokat->pole_zwroc();
			this->obwod=prostokat->obwod_zwroc();
		}

		if (a==b && c==d && a==c && !(angle==90.0)){

			Romb *romb = new Romb(a,angle);
			this->pole=romb->pole_zwroc();
			this->obwod=romb->obwod_zwroc();

		}

		string a_4="Dla takich danych nie ma funkcji liczacej pole i obwod ";

		if(this->obwod==0 && this->pole==0){
			cout <<a_4<<endl;
			throw a_4;
		}

	}


class Okrag 
: public Figura{

	private:
	 double r;

	public:

	Okrag(double r){

		string a_1="Promien musi byc wiekszy niz zero ";
		if(r<=0){
			cout <<a_1<<endl;
			throw a_1;
		}

		this->r=r;
		pole_licz();
		obwod_licz();
	}

	private:
	 void pole_licz(){

		this->pole=M_PI*r*r;
	}

	void obwod_licz(){
		this->obwod=2*M_PI*r;

	}

	public:
	double obwod_zwroc(){

		return this->obwod;
	}

	double pole_zwroc(){

		return this->pole;
	}

};

class Pieciokat
: public  Figura {

	private:
	 double r;

	public:

	Pieciokat(double r){

		string a_1="Bok pieciokata musi byc wiekszy niz zero ";

		if(r<=0){
			cout <<a_1<<endl;
			throw a_1;
		}

		this->r=r;
		this->pole_licz();
		this->obwod_licz();
	}

	private:

	void pole_licz(){

		this->pole =((this->r*this->r)*(sqrt(5*(5+2*sqrt(5)))))*0.25;
	}

	void obwod_licz(){

		this->obwod=this->r*5;
	}

	public:

	double obwod_zwroc(){
		return this->obwod;
	}

	double pole_zwroc(){
		return this->pole;
	}

};

class Szesciokat 
: public Figura {

	private:
	 double r;

	public:

	Szesciokat(double r){

		string a_1="Bok szesciokata musi byc wiekszy niz zero ";

		if(r<=0){
			cout <<a_1<<endl;
			throw a_1;
		}

		this->r=r;
		this->pole_licz();
		this->obwod_licz();
	}

	private:

	void pole_licz(){

		this->pole =((this->r*this->r)*sqrt(3))*1.5;
	}

	void obwod_licz(){

		this->obwod=this->r*6;
	}

	public:

	double obwod_zwroc(){
		return this->obwod;
	}

	double pole_zwroc(){
		return this->pole;
	}

};

double parsowanie(const char *a){
	
	string a_2="Niepoprawny argument (tekst lub bok o dlugosci 0";
	double b;
	b=atof(a);
	if (b==0.0){
		cout <<a_2;
		throw a_2;
	}
	else return b;
}




int main(int argc, char const *argv[])
{
	int count =0;
	int temporal=0;
	double f=0,b=0,c=0,d=0,e=0;


	
	string str(argv[1]);
	temporal=str.length();

	char figury[temporal];


	for (int i=0;i<temporal;i++){
		figury[i]=str[i];
	}

	for (int i=0;i<temporal;i++){

			if((figury[i]=='o')||(figury[i]=='p')||(figury[i]=='s'))
				count++;
			if(figury[i]=='c')
				count=count+5;


		} 

		string a_1="Niewystarczajaca ilosc parametrow";

		if(count>argc-3){
			cout << a_1<<endl;
			throw a_1;
		}

		count =2;

		Figura **tab;
		tab=new Figura*[temporal];

		for (int i=0;i<temporal;i++){
		
			if(figury[i]=='o'){

				f=parsowanie(argv[count]);
				Okrag *temp =new Okrag(f);

				tab[i]=temp;
				count++;
				temp->~Okrag();

			}
			else if(figury[i]=='c'){

				f=parsowanie(argv[count]);
				b=parsowanie(argv[count+1]);
				c=parsowanie(argv[count+2]);
				d=parsowanie(argv[count+3]);
				e=parsowanie(argv[count+4]);
				Czworokat *temp = new Czworokat();
				temp->czworokat_maker(f,b,c,d,e);

				tab[i]=temp;
				count=count+5;
				temp->~Czworokat();
			}
			else if(figury[i]=='p'){
				f=parsowanie(argv[count]);
				Pieciokat *temp = new Pieciokat(f);

				tab[i]=temp;
				count++;
				temp->~Pieciokat();
			}
			else if (figury[i]=='s'){
				f=parsowanie(argv[count]);
				Szesciokat *temp = new Szesciokat(f);

				tab[i]=temp;
				count++;
				temp->~Szesciokat();
			}

		}


		for (int i=0;i<temporal;i++){

			cout << "Pole " << i+1 << " figury wynosi "<<tab[i]->pole_zwroc()<<endl;
			cout << "Obwod " << i+1 << " figury wynosi "<<tab[i]->obwod_zwroc()<<endl;

		}

		delete[] tab;


	return 0;

}

