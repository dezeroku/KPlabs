/** GUI Class for printing Pascal's triangle
@author d0ku (Jakub Piątkowski)
@version 1.7 (beta)
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class MyWindowsListener implements WindowListener {

	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	public void windowDeactivated(WindowEvent e) {
	};

	public void windowActivated(WindowEvent e) {
	};

	public void windowDeiconified(WindowEvent e) {
	};

	public void windowIconified(WindowEvent e) {
	};

	public void windowClosed(WindowEvent e) {
	};

	public void windowOpened(WindowEvent e) {
	};
}

class DisplayButtonListener implements ActionListener {

	Variables source;
	JTextField which_verse;
	JFrame okienko;
	JPanel okieneczko;

	public void actionPerformed(ActionEvent e) {
		Boolean succes = true;

		if (this.source.delete_begin != 0) {
			int a = okieneczko.getComponentCount();

			for (int i = a - 1; i > this.source.delete_begin - 1; i--) {
				okieneczko.remove(i);
			}

			okieneczko.revalidate();
			okieneczko.repaint();
			okienko.pack();
		}

		try {
			this.source.verse_number = Integer.parseInt(this.source.text);
		} catch (NumberFormatException ex) {
			this.which_verse.setText("Argument musi byc liczba naturalna");
			succes = false;
		}

		if (this.source.verse_number <= 0) {
			succes = false;
			this.which_verse.setText("Argument musi byc liczba dodatnia");
			this.source.verse_number = 1;
		}

		if (this.source.verse_number >= 34) {
			succes = false;
			this.which_verse.setText("Zbyt wysoki argument :(");
			this.source.verse_number = 1;
		}

		if (succes == true) {
			this.source.delete_begin = okieneczko.getComponentCount();

			for (int i = 0; i < this.source.verse_number; i++) {
				WierszTrojkataPascala temp = new WierszTrojkataPascala(i);
				JTextField triangle_line = new JTextField(temp.wiersz()
						.length() - 3);

				triangle_line.setText(temp.wiersz());
				Font font= new Font("SansSerif", Font.PLAIN, 8);
				if(i>=27)
					triangle_line.setFont(font);
				triangle_line.setHorizontalAlignment(JTextField.CENTER);
				okieneczko.add(triangle_line, okieneczko.getComponentCount());
				temp = null;

			}

			okienko.pack();

			okienko.revalidate();
			okienko.repaint();
		}
		;
	}

	DisplayButtonListener(Variables source, JTextField which_verse,
			JFrame okienko, JPanel okieneczko) {
		this.source = source;
		this.which_verse = which_verse;
		this.okienko = okienko;
		this.okieneczko = okieneczko;
	}

}

class HowMuchItemListener implements ItemListener {
	Variables source;

	public void itemStateChanged(ItemEvent e) {
	}

	HowMuchItemListener(Variables source) {
		this.source = source;
	}

}

class WhichVerseFocusListener implements FocusListener {
	Variables source;
	JTextField temp;

	public void focusLost(FocusEvent e) {
		this.source.text = this.temp.getText();
	}

	public void focusGained(FocusEvent e){};

	WhichVerseFocusListener(Variables source,JTextField temp) {
		this.source = source;
		this.temp=temp;
	}

}

class Variables {

	String text = " ";
	int verse_number = 1;
	int delete_begin = 0;

}

class Test_GUI {

	public static void main(String[] args) {

		Variables variables = new Variables();
		JFrame okienko = new JFrame("Wyswietlanie wiersza Pascala");
		JPanel okieneczko = new JPanel();
		okieneczko.setLayout(new BoxLayout(okieneczko, BoxLayout.PAGE_AXIS));
		// okieneczko.setBounds(600,400,400,600);
		okieneczko.setVisible(true);
		okienko.add(okieneczko);

		// okienko.setLayout(new BoxLayout(okienko,BoxLayout.PAGE_AXIS));
		// okienko.setLayout(new FlowLayout());
		okienko.addWindowListener(new MyWindowsListener());
		okienko.setBounds(600, 400, 400, 600);

		Choice how_much = new Choice();
		how_much.addItemListener(new HowMuchItemListener(variables));
		how_much.add("1");
		how_much.add("2");
		how_much.add("3");
		how_much.add("4");
		JTextField which_verse = new JTextField("Tu wprowadz numer wiersza", 22);
		which_verse.setHorizontalAlignment(JTextField.CENTER);
		which_verse.addFocusListener(new WhichVerseFocusListener(variables,which_verse));

		Button display_button = new Button("Wyswietl wiersz");
		display_button.addActionListener(new DisplayButtonListener(variables,
				which_verse, okienko, okieneczko));

		okieneczko.add(which_verse);
		// okienko.add(how_much);
		okieneczko.add(display_button);

		okienko.pack();
		okienko.setLocationRelativeTo(null);gh
		okienko.setVisible(true);

	}

}