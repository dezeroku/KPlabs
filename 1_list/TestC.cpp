#include <iostream>
#include <sstream>
#include "WierszTrojkataPascalaC.cpp"
using namespace std;


int main(int argc, char *argv[])
{
	int n=0,z=0,tmp=0;
	stringstream s;
	int g;
	s<<argv[1];
	s>>n;
	if (s.fail()){
		cout << ">Zly typ argumentu"<<endl;
		return 1;
	}
	for (int i=1;i<argc;i++){
		s.clear();
		s.str(argv[i]);
		s>>n;
		if (i==1){
			if (n>=34){
				string wyjatek1="Zbyt wysoki numer wiersza";
				cout <<wyjatek1 <<endl;
				throw wyjatek1;
			}

			if (n<0){
				string wyjatek2="Nie istnieje wiersz o takim indeksie";
				cout << wyjatek2 << endl;
			 throw wyjatek2;
			}
			z=n;
		}

 		WierszTrojkataPascalaC *action= new WierszTrojkataPascalaC(z);
			
 		if (i==1)
 			continue;

		if (s.fail()){
			cout << "Zly typ argumentu"<<endl;
			continue;
		}
		
 		tmp=action->wspolczynnik(n);

		if (tmp==0)
			return 0;

		cout<< n << " -> " <<tmp<< "\n";

		action->~WierszTrojkataPascalaC();

	}

	return 0;
}