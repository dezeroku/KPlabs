import java.net.*;
import java.io.*;

class SocketServer
{
    ServerSocket server = null;
    Socket client = null;
    BufferedReader in = null;
    PrintWriter out = null;
    String line = "";
    Bst<String> stringtree = new Bst<String>();
    Bst<Integer> integertree = new Bst<Integer>();
    Bst<Double> doubletree = new Bst<Double>();

    // int treetype=0; //0 int, 1 double , 2 String


    SocketServer()
    {
        try
        {
            server = new ServerSocket(4444);
        }
        catch (IOException e)
        {
            System.out.println("Could not listen on port 4444");
            System.exit(-1);
        }

    }

    public void listenSocket()
    {
        try
        {
            client = server.accept();
        }
        catch (IOException e)
        {
            System.out.println("Accept failed: 4444");
            System.exit(-1);
        }
        try
        {
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            out = new PrintWriter(client.getOutputStream(), true);
        }
        catch (IOException e)
        {
            System.out.println("Accept failed: 4444");
            System.exit(-1);
        }
        while(line != null)
        {
            try
            {
                line = in.readLine();
                System.out.println(line);
                String[] commands = line.split(":");
                Boolean tempbool;
                int temptype = Integer.parseInt(commands[1]); //komenda+typ+dana

                if(commands[2].compareTo("TURNOFFSERVER") == 0)
                {
                    out.println("Serwer wyłączony");
                    this.finalize();
                }

                switch(commands[0])
                {

                case "i": //insert
                {
                    if(commands.length < 3)
                    {
                        line = "Wprowadź liczbę do wysłania";
                        temptype = -1;
                        break;
                    }

                    if(temptype == 0)
                    {
                        try
                        {
                            int temp = Integer.parseInt(commands[2]);
                            this.integertree.insert(new Integer(commands[2]));
                        }
                        catch(Exception e)
                        {
                            line = "Wprowadź poprawną daną";
                        }
                        line = this.integertree.drawToString();

                    }
                    if(temptype == 1)
                    {
                        try
                        {
                            double temp = Double.parseDouble(commands[2]);
                            this.doubletree.insert(new Double(commands[2]));
                        }
                        catch(Exception e)
                        {
                            line = "Wprowadź poprawną daną";
                        }
                        line = this.doubletree.drawToString();

                    }
                    if(temptype == 2)
                    {
                        try
                        {
                            this.stringtree.insert(commands[2]);
                        }
                        catch(Exception e)
                        {
                            line = "Wprowadź poprawną daną";
                        }
                        line = this.stringtree.drawToString();

                    }
                    break;
                }

                case "d": //delete
                {
                    if(commands.length < 3)
                    {
                        line = "Wprowadź liczbę do wysłania";
                        temptype = -1;
                        break;
                    }

                    if(temptype == 0)
                    {
                        try
                        {
                            int temp = Integer.parseInt(commands[2]);
                            this.integertree.delete(new Integer(commands[2]));
                        }
                        catch(Exception e)
                        {
                            line = "Wprowadź poprawną daną";
                        }
                        line = this.integertree.drawToString();
                    }
                    if(temptype == 1)
                    {
                        try
                        {
                            double temp = Double.parseDouble(commands[2]);
                            this.doubletree.delete(new Double(commands[2]));
                            line = this.doubletree.drawToString();
                        }
                        catch(Exception e)
                        {
                            line = "Wprowadź poprawną daną";
                        }
                    }
                    if(temptype == 2)
                    {
                        try
                        {
                            this.stringtree.delete(commands[2]);
                            line = this.stringtree.drawToString();
                        }
                        catch(Exception e)
                        {
                            line = "Wprowadź poprawną daną";
                        }
                    }
                    break;

                }

                case "p":  //draw
                {
                    if(temptype == 0)
                    {

                        line = this.integertree.drawToString();
                    }
                    if(temptype == 1)
                    {

                        line = this.doubletree.drawToString();
                    }
                    if(temptype == 2)
                    {

                        line = this.stringtree.drawToString();
                    }


                    break;
                }

                case "s": //search
                {
                    if(commands.length < 3)
                    {
                        line = "Wprowadź liczbę do wysłania";
                        temptype = -1;
                        break;
                    }

                    if(temptype == 0)
                    {
                        try
                        {
                            int temp = Integer.parseInt(commands[2]);
                            tempbool = this.integertree.search(new Integer(commands[2]));
                        }
                        catch(Exception e)
                        {
                            line = "Wprowadź poprawną daną";
                            break;
                        }
                        if(tempbool == true)
                        {
                            line = commands[2] + " występuje";
                        }
                        else
                        {
                            line = commands[2] + " nie występuje";
                        }
                    }
                    if(temptype == 1)
                    {
                        try
                        {
                            double temp = Double.parseDouble(commands[2]);
                            tempbool = this.doubletree.search(new Double(commands[2]));
                        }
                        catch(Exception e)
                        {
                            line = "Wprowadź poprawną daną";
                            break;
                        }
                        if(tempbool == true)
                        {
                            line = commands[2] + " występuje";
                        }
                        else
                        {
                            line = commands[2] + " nie występuje";
                        }
                    }
                    if(temptype == 2)
                    {

                        tempbool = this.stringtree.search(commands[2]);
                        if(tempbool == true)
                        {
                            line = commands[2] + " występuje";
                        }
                        else
                        {
                            line = commands[2] + " nie występuje";
                        }
                    }

                    break;
                }

                }

                out.println(line);
            }
            catch (IOException e)
            {
                System.out.println("Read failed");
                System.exit(-1);
            }
        }
    }

    public static void main (String[] args)
    {
        SocketServer server = new SocketServer();
        server.listenSocket();
    }

    protected void finalize()
    {
        try
        {
            in.close();
            out.close();
            client.close();
            server.close();
        }
        catch (IOException e)
        {
            System.out.println("Could not close.");
            System.exit(-1);
        }
    }

}