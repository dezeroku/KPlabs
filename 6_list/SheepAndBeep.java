import java.util.Random;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;

/**
 * class Wolf, determinates whole Wolf behavior
 * 
 * @author d0ku
 * @version 1.1
 */
class Wolf implements Runnable {
	private Map board;
	private int x;
	private int y;
	private Random randomgenerator;
	private int previous_x;
	private int previous_y;
	private Boolean sthlives = true;
	private int latencymultiplier = 0;

	/**
	 * Sets the wolf animal
	 * @param  board Map object to move on to
	 * @param  randomgenerator generates random directional if needed
	 * @param  x x location on board
	 * @param  y y location on board
	 * @param  latencymultiplier defines how fast should be the movement of wolf
	 */
	Wolf(Map board, Random randomgenerator, int x, int y, int latencymultiplier) {
		if (latencymultiplier < 0)
			throw new IllegalArgumentException(
					"Opóźnienie mniejsze niż zero nie jest akceptowalne");
		this.board = board;
		this.latencymultiplier = latencymultiplier;
		this.x = x;
		this.y = y;
		this.randomgenerator = randomgenerator;
		board.setMapNumber(this.x, this.y, -1);
	}

	/**
	 * That function determinates where is rabbit closest to wolf
	 * 
	 * @return direction
	 */
	private int findClosestRabbit() {

		int tempx, tempy;
		tempx = -1;
		tempy = -1;
		int temporalx = 0;
		int temporaly = 0;

		Boolean end = false;

		for (int i = 1; i < Math.max(board.getX(), board.getY()) + 1; i = i + 1) {
			for (int k = 1; k < i; k++) {

				temporalx = this.x;
				temporaly = this.y;
				for (int j = 0; j < i * 2 + 2; j++) {

					if (temporalx + j < board.getX())
						if (board.getMapNumber(temporalx + j, temporaly) != -1) {

							if (board.getMapNumber(temporalx + j, temporaly) != 0) {
								tempx = temporalx + j;
								tempy = temporaly;
								end = true;
								break;
							}

						}

					if (temporalx - j >= 0)
						if (board.getMapNumber(temporalx - j, temporaly) != -1) {
							if (board.getMapNumber(temporalx - j, temporaly) != 0) {
								tempx = temporalx - j;
								tempy = temporaly;
								end = true;
								break;
							}

						}

				}

				if (end)
					break;

				temporalx = this.x;
				if (this.y + k < board.getY())
					temporaly = this.y + k;

				for (int j = 0; j < i * 2 + 2; j++) {

					if (temporalx + j < board.getX())
						if (board.getMapNumber(temporalx + j, temporaly) != -1) {

							if (board.getMapNumber(temporalx + j, temporaly) != 0) {
								tempx = temporalx + j;
								tempy = temporaly;
								end = true;
								break;
							}

						}

					if (temporalx - j >= 0)
						if (board.getMapNumber(temporalx - j, temporaly) != -1) {
							if (board.getMapNumber(temporalx - j, temporaly) != 0) {
								tempx = temporalx - j;
								tempy = temporaly;
								end = true;
								break;
							}

						}
				}

				if (end)
					break;

				temporalx = this.x;
				if (this.y - k >= 0)
					temporaly = this.y - k;
				for (int j = 0; j < i * 2 + 2; j++) {

					if (temporalx + j < board.getX())
						if (board.getMapNumber(temporalx + j, temporaly) != -1) {

							if (board.getMapNumber(temporalx + j, temporaly) != 0) {
								tempx = temporalx + j;
								tempy = temporaly;
								end = true;
								break;
							}

						}

					if (temporalx - j >= 0)
						if (board.getMapNumber(temporalx - j, temporaly) != -1) {
							if (board.getMapNumber(temporalx - j, temporaly) != 0) {
								tempx = temporalx - j;
								tempy = temporaly;
								end = true;
								break;
							}

						}

				}

				if (end)
					break;
			}
			if (end)
				break;
		}

		if (tempy != -1 && tempx != -1)
			if (this.x < tempx) {
				if (this.y < tempy) {
					return 7;
				} else if (this.y == tempy) {
					return 4;
				} else if (this.y > tempy) {
					return 2;
				}
			} else if (this.x == tempx) {
				if (this.y < tempy) {
					return 6;
				} else if (this.y == tempy) {

				} else if (this.y > tempy) {

					return 1;
				}
			} else if (this.x > tempx) {
				if (this.y < tempy) {
					return 5;
				} else if (this.y == tempy) {
					return 3;
				} else if (this.y > tempy) {
					return 0;
				}
			}

		return -1;
	}

	/**
	 * Moves in chosen direction
	 * 
	 * @param a
	 *            direction to move
	 */
	private void move(int a) {

		switch (a) {

		case 0: {
			this.x = this.x - 1;
			this.y = this.y - 1;
			break;
		}

		case 1: {
			this.y = this.y - 1;
			break;
		}

		case 2: {
			this.x = this.x + 1;
			this.y = this.y - 1;
			break;
		}
		case 3: {
			this.x = this.x - 1;
			break;
		}
		case 4: {
			this.x = this.x + 1;
			break;
		}
		case 5: {
			this.x = this.x - 1;
			this.y = this.y + 1;
			break;
		}
		case 6: {
			this.y = this.y + 1;
			break;
		}
		case 7: {
			this.x = this.x + 1;
			this.y = this.y + 1;
			break;
		}
		}
		board.setMapNumber(previous_x, previous_y, 0);

		board.setMapNumber(this.x, this.y, -1);

		previous_y = y;
		previous_x = x;

	}

	/**
	 * Determinates wolf behavior
	 */
	public void run() {

		while (sthlives) {
			Thread.yield();

			previous_x = this.x;
			previous_y = this.y;

			if (board.isEaten() == true) {
				try {
					Thread.sleep((long) (1 * 5 * latencymultiplier * (randomgenerator
							.nextDouble() + 0.5)));
				} catch (Exception ex) {
					System.out.println("ex");
				}
				board.setEaten(false);

			}

			if (findClosestRabbit() != -1)
				move(findClosestRabbit());
			else {
				sthlives = false;
				board.setNeedToPrint(false);
				System.out.println("Koniec?");
			}

			try {
				Thread.sleep((long) (1 * latencymultiplier * (randomgenerator
						.nextDouble() + 0.5)));
			} catch (Exception ex) {
				System.out.println("ex");
			}
		}
	}
}

/**
 * class Rabbit, determinates rabbit behavior
 * 
 * @author d0ku
 * @version 1.1
 */
class Rabbit implements Runnable {
	private Map board;
	private int index;
	private int x;
	private int y;
	private int previous_x;
	private int previous_y;
	private Random randomgenerator;
	private Boolean cont = true;
	private Boolean alive = true;
	private int latencymultiplier = 0;


	/**
	 * Sets the Rabbit animal
	 * @param  board Map object to move on to
	 * @param  index number the rabbit would have
	 * @param  x x location on board
	 * @param  y y location on board
	 * @param  randomgenerator generates random directional if needed
	 * @param  latencymultiplier defines how fast should be the movement of rabbit
	 */
	Rabbit(Map board, int index, int x, int y, Random randomgenerator,
			int latencymultiplier) {
		if (latencymultiplier < 0)
			throw new IllegalArgumentException(
					"Opóźnienie mniejsze niż zero nie jest akceptowalne");
		this.latencymultiplier = latencymultiplier;
		this.board = board;
		this.index = index;
		this.x = x;
		this.y = y;
		previous_x = x;
		previous_y = y;
		board.setMapNumber(x, y, index);
		this.randomgenerator = randomgenerator;

	}

	/**
	 * Move function
	 * 
	 * @param a
	 *            direction to move
	 */
	private void move(int a) {

		switch (a) {

		case 0: {
			this.x = this.x - 1;
			this.y = this.y - 1;
			break;
		}

		case 1: {
			this.y = this.y - 1;
			break;
		}

		case 2: {
			this.x = this.x + 1;
			this.y = this.y - 1;
			break;
		}
		case 3: {
			this.x = this.x - 1;
			break;
		}
		case 4: {
			this.x = this.x + 1;
			break;
		}
		case 5: {
			this.x = this.x - 1;
			this.y = this.y + 1;
			break;
		}
		case 6: {
			this.y = this.y + 1;
			break;
		}
		case 7: {
			this.x = this.x + 1;
			this.y = this.y + 1;
			break;
		}
		}

		if (board.getMapNumber(previous_x, previous_y) != index) {
			alive = false;
			board.setEaten(true);

		}

		if (board.getMapNumber(this.x, this.y) == 0 && alive) {
			board.setMapNumber(previous_x, previous_y, 0);
			board.setMapNumber(this.x, this.y, index);
		} else {
			this.x = previous_x;
			this.y = previous_y;
		}

		if (board.getMapNumber(this.x, this.y) != index) {
			alive = false;
			board.setEaten(true);

		}

		previous_y = y;
		previous_x = x;

	}

	/**
	 * behavior of Rabbit
	 */
	public void run() {

		while (alive) {
			Thread.yield();

			if (board.getMapNumber(this.x, this.y) != index) {
				board.setEaten(true);
				alive = false;
				break;
			}

			if ((this.x == 0 || this.y == 0 || this.x == board.getX() - 1 || this.y == board
					.getY() - 1) && (alive)) {

				if (this.x == 0 && this.y == 0 && alive) {
					switch (randomgenerator.nextInt(3)) {
					case 0: {
						move(4);
						break;
					}
					case 1: {
						move(7);
						break;
					}
					case 2: {
						move(6);
						break;
					}
					}
				} else if (this.x == 0 && this.y == board.getY() - 1 && alive) {
					switch (randomgenerator.nextInt(3)) {
					case 0: {
						move(1);
						break;
					}
					case 1: {
						move(2);
						break;
					}
					case 2: {
						move(4);
						break;
					}
					}

				}

				else if (this.y == 0 && this.x == board.getX() - 1 && alive) {
					switch (randomgenerator.nextInt(3)) {
					case 0: {
						move(3);
						break;
					}
					case 1: {
						move(5);
						break;
					}
					case 2: {
						move(6);
						break;
					}
					}

				} else if (this.x == board.getX() - 1
						&& this.y == board.getY() - 1 && alive) {
					switch (randomgenerator.nextInt(3)) {
					case 0: {
						move(0);
						break;
					}
					case 1: {
						move(1);
						break;
					}
					case 2: {
						move(3);
						break;
					}
					}

				} else if (this.x == 0 && this.y == board.getY() - 1 && alive) {
					switch (randomgenerator.nextInt(5)) {
					case 0: {
						move(1);
						break;
					}
					case 1: {
						move(2);
						break;
					}
					case 2: {
						move(4);
						break;
					}
					}

				}

				else if (this.x == 0 && alive) {
					switch (randomgenerator.nextInt(5)) {
					case 0: {
						move(1);
						break;
					}
					case 1: {
						move(2);
						break;
					}
					case 2: {
						move(4);
						break;
					}
					case 3: {
						move(6);
						break;
					}
					case 4: {
						move(7);
						break;
					}
					}

				}

				else if (this.x == board.getX() - 1 && alive) {
					switch (randomgenerator.nextInt(5)) {
					case 0: {
						move(0);
						break;
					}
					case 1: {
						move(1);
						break;
					}
					case 2: {
						move(3);
						break;
					}
					case 3: {
						move(5);
						break;
					}
					case 4: {
						move(6);
						break;
					}
					}

				}

				else if (this.y == 0 && alive) {
					switch (randomgenerator.nextInt(5)) {
					case 0: {
						move(3);
						break;
					}
					case 1: {
						move(4);
						break;
					}
					case 2: {
						move(5);
						break;
					}
					case 3: {
						move(6);
						break;
					}
					case 4: {
						move(7);
						break;
					}
					}

				}

				else if (this.y == board.getY() - 1 && alive) {
					switch (randomgenerator.nextInt(5)) {
					case 0: {
						move(3);
						break;
					}
					case 1: {
						move(4);
						break;
					}
					case 2: {
						move(0);
						break;
					}
					case 3: {
						move(1);
						break;
					}
					case 4: {
						move(2);
						break;
					}
					}

				}
			}

			else if (alive) {

				if (this.x < board.getWolfX()) {
					if (this.y < board.getWolfY()) {
						move(0);
					} else if (this.y == board.getWolfY()) {
						move(3);
					} else if (this.y > board.getWolfY()) {
						move(5);
					}
				} else if (this.x == board.getWolfX()) {
					if (this.y > board.getWolfY()) {
						move(6);
					} else if (this.y < board.getWolfY()) {
						move(1);
					}

				} else if (this.x > board.getWolfX()) {
					if (this.y < board.getWolfY()) {
						move(2);
					} else if (this.y == board.getWolfY()) {
						move(4);
					} else if (this.y > board.getWolfY()) {
						move(7);
					}
				}
			}

			try {
				Thread.sleep((long) (1 * latencymultiplier * (randomgenerator
						.nextDouble() + 0.5)));
			} catch (Exception ex) {
				System.out.println("ex");
			}
		}

	}

}

/**
 * class Map, contains the whole terminal version of map
 * 
 * @author d0ku
 * @version 1.2
 */
class Map {
	private int[][] map;
	private int x;
	private int y;
	private Boolean eaten = false;
	private Boolean needtoprint = true;

	/**
	 * Getter
	 * 
	 * @return whether anything moves on the map
	 */
	public Boolean getNeedToPrint() {
		return this.needtoprint;
	}

	/**
	 * Setter
	 * 
	 * @param temp
	 *            true if anything moves on the map, false if not
	 */
	public void setNeedToPrint(Boolean temp) {
		this.needtoprint = temp;
	}

	/**
	 * Sets boolean value,
	 * 
	 * @param temp
	 *            sets true if rabbit is eaten, false if not
	 */
	public void setEaten(Boolean temp) {
		this.eaten = temp;
	}

	/**
	 * Gets boolean value,
	 * 
	 * @return returns true if rabbit is eaten, false if not
	 */
	public Boolean isEaten() {
		return this.eaten;
	}

	/**
	 * Returns the X size of map
	 * 
	 * @return X size of map
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Returns wolf's X coordinate
	 * 
	 * @return wolf's X coordinate
	 */
	public synchronized int getWolfX() {
		for (int i = 0; i < this.x; i++) {
			for (int j = 0; j < this.y; j++) {
				if (map[i][j] == -1)
					return j;
			}
		}

		return 0;
	}

	/**
	 * Returns wolf's Y coordinate
	 * 
	 * @return wolf's Y coordinate
	 */
	public synchronized int getWolfY() {
		for (int i = 0; i < this.x; i++) {
			for (int j = 0; j < this.y; j++) {
				if (map[i][j] == -1)
					return i;
			}
		}

		return 0;
	}

	/**
	 * return the Y length
	 * 
	 * @return Y length
	 */
	public int getY() {
		return this.y;
	}

	Map() {
	}

	/**
	 * Constructor
	 * 
	 * @param n
	 *            y size
	 * @param m
	 *            x size
	 */
	Map(int n, int m) {
		if (n < 1 || m < 1)
			throw new IllegalArgumentException("Za niski wymiar x lub y");
		this.x = m;
		this.y = n;
		this.map = new int[this.x][this.y];

	}

	/**
	 * Used to get that object terminal map array
	 * 
	 * @return map array
	 */
	private int[][] getMap() {
		return this.map;
	}

	/**
	 * Returns map value at specified location
	 * 
	 * @param a
	 *            x coordinate
	 * @param b
	 *            y coordinate
	 * @return map value at (x,y)
	 */
	public synchronized int getMapNumber(int a, int b) {
		return this.map[a][b];
	}

	/**
	 * Setter for exact map element
	 * 
	 * @param a
	 *            x coordinate
	 * @param b
	 *            y coordinate
	 * @param c
	 *            value to be set
	 */
	public synchronized void setMapNumber(int a, int b, int c) {
		this.map[a][b] = c;
	}

	/**
	 * Setter for map
	 * 
	 * @param temp
	 *            terminal map to be set
	 */
	public void setMap(int[][] temp) {
		this.map = temp;
	}

	/**
	 * It prints the whole map in terminal
	 */
	public synchronized void printMap() {
		try {
			for (int i = 0; i < this.y; i++) {
				for (int j = 0; j < this.x; j++) {
					System.out.print(map[j][i]);
				}
				System.out.print(System.lineSeparator());
			}

			System.out.print(System.lineSeparator());

		} catch (Exception ex) {
			System.out.println("print error");
		}
	}

}

/**
 * class Panels, converts the terminal Map object to GUI panel
 * 
 * @author d0ku
 * @version 1.7
 */
class Panels implements Runnable {
	private JPanel[][] array;
	private JPanel[] array_Yaxis;
	private JPanel whole;
	private Map board;
	private int oneblocksize=20;

	/**
	 * Constructor
	 * 
	 * @param board
	 *            Map object containing terminal map
	 */
	Panels(Map board) {
		
		
		Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		JFrame testing = new JFrame();
		testing.setMaximumSize(screensize);
		screensize= testing.getMaximumSize();
		testing.dispose();

		int tempx=(int)screensize.getWidth() ;
		int tempy= (int)screensize.getHeight();

		tempx=tempx/board.getX();
		tempy=tempy/board.getY();

		int temp =  Math.min(tempx,tempy)-2;
		if (temp==0)
			this.oneblocksize=1;
		else this.oneblocksize=temp;

		array = new JPanel[board.getX()][board.getY()];
		array_Yaxis = new JPanel[board.getY()];
		this.board = board;

		for (int i = 0; i < board.getX(); i++) {
			for (int j = 0; j < board.getY(); j++) {
				array[i][j] = new JPanel();
				array[i][j].setBorder(BorderFactory
						.createLineBorder(Color.black));
				array[i][j].setMinimumSize(new Dimension(this.oneblocksize, this.oneblocksize));
				array[i][j].setMaximumSize(new Dimension(this.oneblocksize, this.oneblocksize));

				if (board.getMapNumber(i, j) == 0)
					array[i][j].setBackground(Color.WHITE);
				else if (board.getMapNumber(i, j) == -1)
					array[i][j].setBackground(Color.BLACK);
				else
					array[i][j].setBackground(Color.GRAY);
			}
		}

		for (int j = 0; j < board.getY(); j++) {

			array_Yaxis[j] = new JPanel();
			array_Yaxis[j].setLayout(new FlowLayout());
			array_Yaxis[j].setMinimumSize(new Dimension(this.oneblocksize * board.getX(), this.oneblocksize));
			array_Yaxis[j].setMaximumSize(new Dimension(this.oneblocksize * board.getX(), this.oneblocksize));
			for (int i = 0; i < board.getX(); i++) {

				array_Yaxis[j].add(array[i][j]);
			}

		}

		whole = new JPanel();
		whole.setLayout(new BoxLayout(whole, BoxLayout.PAGE_AXIS));
		whole.setMinimumSize(new Dimension(this.oneblocksize * board.getX(), this.oneblocksize * board.getY()));
		whole.setMaximumSize(new Dimension(this.oneblocksize * board.getX(), this.oneblocksize * board.getY()));

		for (int i = 0; i < board.getY(); i++) {
			whole.add(array_Yaxis[i]);
		}
	}

	/**
	 * refreshes whole GUI map with data from terminal map
	 */
	public void paint() {
		for (int i = 0; i < board.getX(); i++) {

			for (int j = 0; j < board.getY(); j++) {
				if (board.getMapNumber(i, j) == 0)
					array[i][j].setBackground(new Color(0, 209, 20));
				else if (board.getMapNumber(i, j) == -1
						&& board.isEaten() == true)
					array[i][j].setBackground(Color.RED);
				else if (board.getMapNumber(i, j) == -1)
					array[i][j].setBackground(Color.BLACK);
				else
					array[i][j].setBackground(Color.WHITE);
			}

		}
	}

	/**
	 * Method used to get Main Panel
	 * 
	 * @return panel containing whole map
	 */
	public JPanel returnMainPanel() {
		return whole;
	}

	/**
	 * Refreshing the GUI map
	 */
	public void run() {

		while (board.getNeedToPrint()) {

			this.paint();
			whole.revalidate();
			whole.repaint();

			try {
				Thread.sleep(3);
			} catch (Exception ex) {
				System.out.println("ex");
			}
			Thread.yield();
		}
	}
}

/**
 * class SheepandBeep, the main class responsible for testing everything
 * 
 * @author d0ku
 * @version 1.2
 */
public class SheepAndBeep {

	/**
	 * Whole action
	 * 
	 * @param args
	 *            1 count of Rabbits, 2 latency, 3 ysize, 4 xsize
	 */
	public static void main(String[] args) {

		int rabbit_count = 0;
		int latencymultiplier = 0;
		int xsize = 0;
		int ysize = 0;

		if (args.length != 4) {
			System.out.println("Należy wprowadzić cztery argumenty");
			System.out
					.println("liczba zajęcy, opóźnienie, rozmiar y planszy, rozmiar x planszy");
			return;
		}

		try {
			rabbit_count = Integer.parseInt(args[0]);
			latencymultiplier = Integer.parseInt(args[1]);
			xsize = Integer.parseInt(args[2]);
			ysize = Integer.parseInt(args[3]);
		} catch (Exception e) {
			System.out.println("Jeden z argumentów nie jest liczbą...");
			return;
		}

		Random randomgenerator = new Random();
		Map board = new Map();

		try {
			board = new Map(xsize, ysize);
		} catch (Exception exc) {
			System.out.println(exc.getMessage());
			return;
		}

		JFrame window = new JFrame("Wolf and Rabbits");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		window.setVisible(true);
		window.setBounds(600, 600, 600, 600);

		//JInternalFrame internal = new JInternalFrame("SheepAndBeep");
		//internal.setVisible(true);

		//JPanel main = new JPanel(new FlowLayout());

		Panels boardGUI = new Panels(board);
		//main.add(boardGUI.returnMainPanel());
		boardGUI.paint();

		//internal.add(main);

		window.add(boardGUI.returnMainPanel());
		window.pack();

		for (int i = 0; i < rabbit_count; i++) {
			try {
				Rabbit krolik = new Rabbit(board, i + 1,
						randomgenerator.nextInt(board.getX()),
						randomgenerator.nextInt(board.getY()), randomgenerator,
						latencymultiplier);
				new Thread(krolik).start();
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
				return;
			}
		}

		try {
			Wolf wilk = new Wolf(board, randomgenerator,
					randomgenerator.nextInt(board.getX()),
					randomgenerator.nextInt(board.getY()), latencymultiplier);
			new Thread(wilk).start();
		} catch (Exception exce) {
			System.out.println(exce.getMessage());
			return;
		}

		Thread display = new Thread(boardGUI);
		display.setDaemon(true);
		//display.setPriority(Thread.MAX_PRIORITY);

		display.start();
	}
}