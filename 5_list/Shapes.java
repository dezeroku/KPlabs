import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.io.*;

/**
 * class Shapes, it handles draw patterns and contains all paths
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version beta
 */
public class Shapes {
	private DrawingPanel okienko;
	private int mode = 0;
	private int type = 0;
	private final int how_many_figures_possible = 2;
	private Boolean safe = true;
	private int count = 0;
	private GeneralPath[] paths = new GeneralPath[how_many_figures_possible];
	private int[] types = new int[how_many_figures_possible];
	private Color[] linecolors = new Color[how_many_figures_possible];
	private Color[] fullfillments = new Color[how_many_figures_possible];

	/**
	 * Constructor, okienko is panel containing g2d value
	 */
	Shapes(DrawingPanel okienko) {
		this.okienko = okienko;
	}

	/**
	 * Getter for types
	 * 
	 * @param a
	 *            index
	 * @return type of figure at choosen index
	 */
	public int getTypes(int a) {
		return this.types[a];
	}

	/**
	 * Setter for types, sets b type at a index
	 * 
	 * @param a
	 *            index
	 * @param b
	 *            value
	 */
	public void setTypes(int a, int b) {
		this.types[a] = b;
	}

	/**
	 * Getter for line color
	 * 
	 * @param a
	 *            index
	 * @return line color of figure at selected index
	 */
	public Color getLineColor(int a) {
		return this.linecolors[a];
	}

	/**
	 * Setter for line color
	 * 
	 * @param a
	 *            index
	 * @param b
	 *            color to set
	 */
	public void setLineColor(int a, Color b) {
		this.linecolors[a] = b;
	}

	/**
	 * Getter for fill color
	 * 
	 * @param a
	 *            index
	 * @return color of figure at chosen index
	 */
	public Color getFillColor(int a) {
		return this.fullfillments[a];
	}

	/**
	 * Setter for fill color
	 * 
	 * @param a
	 *            index
	 * @param b
	 *            fill color to be set at a index figure
	 */
	public void setFillColor(int a, Color b) {
		this.fullfillments[a] = b;
	}

	/**
	 * Getter for count
	 * 
	 * @return count
	 */
	public int getCount() {
		return this.count;
	}

	/**
	 * Setter for count
	 * 
	 * @param temp
	 *            value to be set as count
	 */
	public void setCount(int temp) {
		this.count = temp;
	}

	/**
	 * Getter, returns current drawn figure type
	 * 
	 * @return figure type
	 */
	public int getType() {
		return type;
	}

	/**
	 * Setter for current drawn figure type
	 * 
	 * @param temp
	 *            figure type
	 */
	public void setType(int temp) {
		type = temp;
	}

	/**
	 * It decides whether it is safe to draw new Figure
	 * 
	 * @return True if safe, false if not safe
	 */
	public Boolean getSafe() {
		return safe;
	}

	/**
	 * Setter, sets, if it safe to draw new Figure
	 * 
	 * @param temp
	 *            true if safe, false if not safe
	 */
	public void setSafe(Boolean temp) {
		safe = temp;
	}

	/**
	 * Getter ,returns current program mode
	 * 
	 * @return 0 if drawing, 1 if modifying
	 */
	public int getMode() {
		return mode;
	}

	/**
	 * Setter, sets current program mode
	 * 
	 * @param temp
	 *            0 if drawing, 1 if modifying
	 */
	public void setMode(int temp) {
		mode = temp;
	}

	/**
	 * Draws all figures lines, but without fill
	 * 
	 * @param g2d
	 *            Graphics2D to draw on
	 */
	public void markAllPaths(Graphics2D g2d) {

		for (int i = 0; i < count; i++) {
			g2d.setColor(linecolors[i]);
			g2d.draw(paths[i]);
			g2d.setColor(fullfillments[i]);
		}
	}

	/**
	 * Draws all figures lines, with fill
	 * 
	 * @param g2d
	 *            Graphics2D to draw on
	 */
	public void drawAllPaths(Graphics2D g2d) {

		g2d.clearRect(0, 0, 1000, 1000);
		for (int i = 0; i < count; i++) {
			g2d.setColor(linecolors[i]);
			g2d.draw(paths[i]);
			g2d.setColor(fullfillments[i]);
			g2d.fill(paths[i]);
			g2d.setColor(fullfillments[i]);
		}
		okienko.revalidate();
		okienko.repaint();
	}

	/**
	 * draws chosen path, with fill
	 * 
	 * @param g2d
	 *            Graphics2D to draw on
	 * @param a
	 *            path index
	 */
	public void drawIndexedPath(Graphics2D g2d, int a) {
		g2d.setColor(linecolors[a]);
		g2d.draw(paths[a]);
		g2d.setColor(fullfillments[a]);
		g2d.fill(paths[a]);
		g2d.setColor(Color.BLACK);
	}

	/**
	 * Draws all figures fills, but without lines
	 * 
	 * @param g2d
	 *            Graphics2D to draw on
	 */
	public void fillAllPaths(Graphics2D g2d) {

		for (int i = 0; i < count; i++) {
			g2d.setColor(fullfillments[i]);
			g2d.fill(paths[i]);
			g2d.setColor(fullfillments[i]);
		}
	}

	/**
	 * Adds provided GeneralPath to paths
	 * 
	 * @param temp
	 *            GeneralPath to be added
	 * @param whichtype
	 *            Figure type
	 * @param drawing
	 *            Color of Lines
	 * @param fullfillment
	 *            Color of Fill
	 */
	public void addGeneralPath(GeneralPath temp, int whichtype, Color drawing,
			Color fullfillment) {
		if (count == how_many_figures_possible){
			count = 0;
            this.safe=false;
            paths[0]=null;
            
        }

		this.paths[count] = temp;
		this.types[count] = whichtype;
		this.linecolors[count] = drawing;
		this.fullfillments[count] = fullfillment;
		count++;
	}

	/**
	 * returns path with provided index
	 * 
	 * @param a
	 *            index
	 * @return path at that index
	 */
	public GeneralPath indexedPath(int a) {
		return paths[a];
	}

	/**
	 * sets provided path at provided index
	 * 
	 * @param temp
	 *            GeneralPath to be set
	 * @param index
	 *            index
	 */
	public void setIndexedPath(GeneralPath temp, int index) {
		this.paths[index] = temp;

	}

	/**
	 * backups all stored paths into ./BackedupPaths.txt file
	 */
	public void backupAllPaths() {
		try {
			FileOutputStream file = new FileOutputStream(new File(
					"BackedupPaths.txt"));
			ObjectOutputStream object = new ObjectOutputStream(file);

			Integer tempint = new Integer(count);
			object.writeObject(tempint);
			for (int i = 0; i < count; i++) {
				object.writeObject(paths[i]);
				object.writeObject(fullfillments[i]);
				object.writeObject(linecolors[i]);
				object.writeObject(new Integer(types[i]));
			}

			object.close();
			file.close();
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(okienko,
					"Nie udało się zapisać ekranu na dysku ", "Błąd zapisu",
					JOptionPane.WARNING_MESSAGE);
		}

	}

	/**
	 * loads all paths stored in ./BackedupPaths.txt (if file exists)
	 */
	public void loadAllPaths() {
		Boolean check = false;
		try {
			File file_check = new File("BackedupPaths.txt");
			check = file_check.exists();
			file_check = null;
		} catch (Exception exc) {
			
			JOptionPane.showMessageDialog(okienko,
					"Plik z kopią ekranu nie istnieje", "Błąd wczytywania",
					JOptionPane.WARNING_MESSAGE);
		}
		if (check == true) {

			try {

				FileInputStream file = new FileInputStream(new File(
						"BackedupPaths.txt"));

				ObjectInputStream object = new ObjectInputStream(file);

				int temp = ((Integer) object.readObject()).intValue();
				count = temp;
				for (int i = 0; i < temp; i++) {

					paths[i] = null;
					paths[i] = (GeneralPath) object.readObject();

					fullfillments[i] = null;
					fullfillments[i] = (Color) object.readObject();

					linecolors[i] = null;
					linecolors[i] = (Color) object.readObject();

					types[i] = 0;
					types[i] = ((Integer) object.readObject()).intValue();

				}

				object.close();
				file.close();
				Graphics2D temporalgraph = okienko.getG2d();
				this.drawAllPaths(temporalgraph);
				okienko.setG2d(temporalgraph);
				okienko.revalidate();
				okienko.repaint();
			}

			catch (Exception ex) {
				JOptionPane.showMessageDialog(okienko,
						"Wczytywanie wszystkich ścieżek nieudane",
						"Błąd wczytywania", JOptionPane.WARNING_MESSAGE);
			}

		}
		else 
			JOptionPane.showMessageDialog(okienko,
					"Plik z kopią ekranu nie istnieje", "Błąd wczytywania",
					JOptionPane.WARNING_MESSAGE);

	}

	/**
	 * loads one path from provided location
	 * 
	 * @param location
	 *            path to file
	 */
	public void loadOnePath(String location) {

		Boolean check = false;
		try {
			File file_check = new File("BackedupPaths.txt");
			check = file_check.exists();
			file_check = null;
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(okienko, "Taki plik nie istnieje",
					"Błąd wczytywania", JOptionPane.WARNING_MESSAGE);

		}
		if (check == true) {
			try {

				FileInputStream file = new FileInputStream(new File(location));
				ObjectInputStream object = new ObjectInputStream(file);

				paths[count] = null;
				paths[count] = (GeneralPath) object.readObject();

				fullfillments[count] = null;
				fullfillments[count] = (Color) object.readObject();

				linecolors[count] = null;
				linecolors[count] = (Color) object.readObject();

				types[count] = 0;
				types[count] = ((Integer) object.readObject()).intValue();
				System.out.println("loaded");

				object.close();
				file.close();
				count++;
				Graphics2D temporalgraph = okienko.getG2d();
				this.drawAllPaths(temporalgraph);
				okienko.setG2d(temporalgraph);
				okienko.revalidate();
				okienko.repaint();

			} catch (Exception ex) {
				JOptionPane.showMessageDialog(okienko,
						"Wczytywanie pojedynczej ścieżki nieudane",
						"Błąd wczytywania", JOptionPane.WARNING_MESSAGE);

			}
		}

	}

	/**
	 * saves last path to provided location
	 * 
	 * @param location
	 *            provided location
	 */
	public void saveLastPath(String location) {

		if (count > 0) {
			try {
				FileOutputStream file = new FileOutputStream(new File(location));
				ObjectOutputStream object = new ObjectOutputStream(file);

				object.writeObject(paths[count - 1]);
				object.writeObject(fullfillments[count - 1]);
				object.writeObject(linecolors[count - 1]);
				object.writeObject(new Integer(types[count - 1]));

				object.close();
				file.close();

			} catch (Exception ex) {
				JOptionPane.showMessageDialog(okienko,
						"Nie udało się zapisać figury :(", "Błąd zapisu",
						JOptionPane.WARNING_MESSAGE);
			}
		}

	}

	/**
	 * Creates oval path from provided points
	 * 
	 * @param points
	 *            provided points
	 * @return GeneralPath containing oval from provided points
	 */
	public GeneralPath makeNewGeneralPathOval(Point[] points) {

		double tempx1, tempx2, tempy1, tempy2, range;
		tempx1 = tempx2 = tempy1 = tempy2 = range = 0;
		tempx1 = points[0].getX();
		tempx2 = points[1].getX();
		tempy1 = points[0].getY();
		tempy2 = points[1].getY();

		range = Math.sqrt(Math.pow(Math.abs(tempx1 - tempx2), 2)
				+ Math.pow(Math.abs(tempy1 - tempy2), 2));

		Ellipse2D.Double temp = new Ellipse2D.Double(tempx1 - range, tempy1
				- range, range * 2, range * 2);

		GeneralPath ovalpath = new GeneralPath(temp);

		return ovalpath;

	}

	/**
	 * Creates Rectangle path from provided points
	 * 
	 * @param points
	 *            provided points
	 * @return GeneralPath containing Rectangle from provided points
	 */
	public GeneralPath makeNewGeneralPathRectangle(Point[] points) {

		Rectangle temp;
		double tempx1, tempx2, tempy1, tempy2, range;
		tempx1 = tempx2 = tempy1 = tempy2 = 0;
		tempx1 = points[0].getX();
		tempx2 = points[1].getX();
		tempy1 = points[0].getY();
		tempy2 = points[1].getY();
		if (tempx1 > tempx2) {
			range = tempx2;
			tempx2 = tempx1;
			tempx1 = range;
		}
		if (tempy1 > tempy2)
			temp = new Rectangle((int) tempx1, (int) tempy2,
					(int) Math.abs(tempx1 - tempx2), (int) Math.abs(tempy1
							- tempy2));
		else
			temp = new Rectangle((int) tempx1, (int) tempy1,
					(int) Math.abs(tempx1 - tempx2), (int) Math.abs(tempy1
							- tempy2));
		GeneralPath rectanglepath = new GeneralPath(temp);

		return rectanglepath;
	}
}